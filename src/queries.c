#include "queries.h"

const char *qry_schema =
    "CREATE TABLE IF NOT EXISTS accounts(\n"
    "    id INTEGER PRIMARY KEY, \n"
    "    name TEXT NOT NULL UNIQUE, \n"
    "    username TEXT NOT NULL, \n"
    "    password TEXT NOT NULL, \n"
    "    description TEXT NOT NULL, \n"
    "    iv TEXT NOT NULL\n"
    ");"
    "CREATE TABLE IF NOT EXISTS masterpw (\n"
    "    id INTEGER PRIMARY KEY, \n"
    "    hash TEXT NOT NULL, \n"
    "    salt TEXT NOT NULL\n"
    ");"
    "CREATE UNIQUE INDEX IF NOT EXISTS idx_accounts_name ON accounts(name);";

const char *qry_select_master_pw = "SELECT hash, salt FROM masterpw LIMIT 1;";
const char *qry_insert_master_pw =
    "INSERT INTO masterpw (hash, salt) VALUES (?, ?);";

const char *qry_insert_account =
    "INSERT INTO accounts (name, username, "
    "password, description, iv) VALUES (?, ?, ?, ?, ?);";

const char *qry_delete_account = "DELETE FROM accounts WHERE name = ?;";

const char *qry_get_account_by_id = "SELECT * FROM accounts WHERE id = ?;";
const char *qry_get_account_by_name = "SELECT * FROM accounts WHERE name = ?;";
const char *qry_get_accounts_all = "SELECT * FROM accounts;";
const char *qry_get_accounts_name_all = "SELECT name FROM accounts;";

const char *qry_update_account = "";

const char *qry_select_table_count =
    "SELECT name FROM sqlite_master WHERE tyep='table';";

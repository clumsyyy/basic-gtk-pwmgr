#include "pwmgr.h"
#include "src/account.h"
#include "src/queries.h"

void pwmgr_list_box_populate(Pwmgr *pwmgr) {
    sqlite3_stmt *stmt;
    int result =
        sqlite3_prepare_v2(pwmgr->db, qry_get_accounts_name_all,
                           strlen(qry_get_accounts_name_all), &stmt, NULL);
    if (result != SQLITE_OK) {
        printf("%s\n", sqlite3_errmsg(pwmgr->db));
    }

    while (result != SQLITE_DONE) {
        result = sqlite3_step(stmt);
        if (result == SQLITE_ROW) {
            char *name = (char *)sqlite3_column_text(stmt, 0);
            printf("Adding %s\n", name);
            GtkWidget *row = gtk_list_box_row_new();
            g_signal_connect(row, "activate", G_CALLBACK(pwmgr_list_box_select),
                             pwmgr);
            gtk_container_add(GTK_CONTAINER(row), gtk_label_new(name));
            gtk_container_add(GTK_CONTAINER(pwmgr->list_box), row);
            // gtk_list_box_row_set_activatable(GTK_LIST_BOX_ROW(row), 1);
        }
    }

    result = sqlite3_finalize(stmt);
    if (result != SQLITE_OK) {
        printf("%s\n", sqlite3_errmsg(pwmgr->db));
    }
}

void pwmgr_list_box_select(GtkListBox *self, GtkListBoxRow *row,
                           gpointer data) {

    printf("pwmgr_list_box_select\n");
    if (!row)
        printf("row is null\n");
    if (!self)
        printf("self is null\n");

    Pwmgr *pwmgr = (Pwmgr *)data;
    if (!pwmgr)
        printf("Failed to cast gpointer to usable\n");

    GtkWidget *label = gtk_bin_get_child(GTK_BIN(row));
    if (!label)
        printf("getting label failed\n");
    const char *text = gtk_label_get_text(GTK_LABEL(label));

    sqlite3_stmt *stmt;
    int result =
        sqlite3_prepare_v2(pwmgr->db, qry_get_account_by_name,
                           strlen(qry_get_account_by_name), &stmt, NULL);
    if (result != SQLITE_OK) {
        printf("%s\n", sqlite3_errmsg(pwmgr->db));
    }

    result = sqlite3_bind_text(stmt, 1, text, strlen(text), NULL);
    if (result != SQLITE_OK) {
        printf("%s\n", sqlite3_errmsg(pwmgr->db));
    }

    result = sqlite3_step(stmt);
    if (result != SQLITE_ROW) {
        printf("%s\n", sqlite3_errmsg(pwmgr->db));
    }

    const char *name = (char *)sqlite3_column_text(stmt, 1);
    const char *un = (char *)sqlite3_column_text(stmt, 2);
    const char *pw = (char *)sqlite3_column_text(stmt, 3);
    const char *desc = (char *)sqlite3_column_text(stmt, 4);

    gtk_label_set_text(GTK_LABEL(pwmgr->display_name_label), name);
    gtk_label_set_text(GTK_LABEL(pwmgr->display_un_label), un);
    gtk_label_set_text(GTK_LABEL(pwmgr->display_pw_label), pw);
    gtk_label_set_text(GTK_LABEL(pwmgr->display_desc_label), desc);

    sqlite3_finalize(stmt);
}

#ifndef PWMGR_H
#define PWMGR_H

#include <gtk/gtk.h>
#include <openssl/conf.h>
#include <openssl/err.h>
#include <openssl/evp.h>
#include <sqlite3.h>

typedef struct Pwmgr {
    // GtkWidgets to keep track of
    GtkApplication *app;        // holds main app pointer
    GtkWidget *main_window;     // main window of the application
    GtkWidget *list_box;        // holds accounts?
    GtkWidget *scrolled_window; // not needed?

    // Labels that should almost always display data;
    GtkWidget *display_name_label;
    GtkWidget *display_un_label;
    GtkWidget *display_pw_label;
    GtkWidget *display_desc_label;

    GString *data_dir;
    GString *db_path;
    sqlite3 *db;

    GString *master_hash;
    GString *master_salt;
    GString *master_key;
} Pwmgr;

// constructor and destructor
Pwmgr *pwmgr_init(void);
void pwmgr_free(Pwmgr *);

// common functions
void pwmgr_remove_window_elements(GtkWidget *);
void pwmgr_show_main(Pwmgr *);

void pwmgr_add_account(GtkButton *, gpointer);
void pwmgr_add_account_handler(GtkButton *, gpointer);
void pwmgr_add_account_cancel(GtkButton *, gpointer);

void pwmgr_list_box_populate(Pwmgr *);
void pwmgr_list_box_select(GtkListBox *, GtkListBoxRow *, gpointer);

void pwmgr_master_password_create(Pwmgr *);
void pwmgr_master_password_create_handler(GtkButton *, gpointer);

void pwmgr_unlock(Pwmgr *);
void pwmgr_unlock_handler(GtkButton *, gpointer);

int encrypt(unsigned char *plaintext, int plaintext_len, unsigned char *key,
            unsigned char *iv, unsigned char *ciphertext);
int decrypt(unsigned char *ciphertext, int ciphertext_len, unsigned char *key,
            unsigned char *iv, unsigned char *plaintext);
void decrypt_password_handler(GtkButton *btn, gpointer data);

/* Structs to represent data passed between GTK_CALLBACK signals */
// pwmgr_account_form
//
// pwmgr_account_form is a struct used to pass a series of GtkEntry fields into
// a G_CALLBACK to be used to create an Account and save it to the sqlite
// database
typedef struct pwmgr_account_form {
    Pwmgr *pwmgr;
    GtkWidget *name_entry;
    GtkWidget *un_entry;
    GtkWidget *pw_entry;
    GtkWidget *desc_entry;

} pwmgr_account_form;

// pwmgr_create_master_pw_form
//
// pwmgr_create_master_pw_form is a struct used on first application startup
// to prompt the user to create a new master password to be used to open the
// application
typedef struct pwmgr_create_master_pw_form {
    Pwmgr *pwmgr;
    GtkWidget *error_label;
    GtkWidget *entry1;
    GtkWidget *entry2;
} pwmgr_create_master_pw_form;

// pwmgr_unlock_master_pw_form
//
//  pwmgr_unlock_master_pw_form is a struct used on application startup to
//  confirm the user has the correct password
typedef struct pwmgr_unlock_master_pw_form {
    Pwmgr *pwmgr;
    GtkWidget *entry;
    GtkWidget *error_label;
} pwmgr_unlock_master_pw_form;

typedef struct pwmgr_show_pw {
    Pwmgr *pwmgr;
} pwmgr_show_pw;
/* end of structs representing GTK_CALLBACK data */

#endif

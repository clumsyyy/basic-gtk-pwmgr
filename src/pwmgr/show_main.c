#include "pwmgr.h"

void pwmgr_show_main(Pwmgr *pwmgr) {
    printf("pwmgr_show_main()\n");
    pwmgr_remove_window_elements(pwmgr->main_window);

    GtkWidget *main_box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 10);
    GtkWidget *top_box = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 10);
    GtkWidget *bottom_box = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 10);
    gtk_box_pack_start(GTK_BOX(main_box), top_box, 0, 0, 10);
    gtk_box_pack_start(GTK_BOX(main_box), bottom_box, 0, 0, 10);

    GtkWidget *add_btn = gtk_button_new_with_label("Add");
    g_signal_connect(add_btn, "clicked", G_CALLBACK(pwmgr_add_account), pwmgr);

    GtkWidget *del_btn = gtk_button_new_with_label("Delete");
    gtk_box_pack_start(GTK_BOX(top_box), add_btn, 0, 0, 10);
    gtk_box_pack_start(GTK_BOX(top_box), del_btn, 0, 0, 10);

    // Create the bottom box stuff

    // Create displayBox first

    GtkWidget *scrolled_window = gtk_scrolled_window_new(NULL, NULL);

    gtk_scrolled_window_set_min_content_height(
        GTK_SCROLLED_WINDOW(scrolled_window), 400);
    gtk_scrolled_window_set_min_content_width(
        GTK_SCROLLED_WINDOW(scrolled_window), 200);
    pwmgr->scrolled_window = scrolled_window;

    GtkWidget *list_box = gtk_list_box_new();
    g_signal_connect(list_box, "row-selected",
                     G_CALLBACK(pwmgr_list_box_select), pwmgr);
    pwmgr->list_box = list_box;
    gtk_list_box_set_selection_mode(GTK_LIST_BOX(list_box),
                                    GTK_SELECTION_SINGLE);
    pwmgr_list_box_populate(pwmgr);

    GtkWidget *display_box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 10);
    pwmgr->display_un_label = gtk_label_new("");
    pwmgr->display_name_label = gtk_label_new("");
    pwmgr->display_pw_label = gtk_label_new("");
    pwmgr->display_desc_label = gtk_label_new("");

    gtk_container_add(GTK_CONTAINER(display_box), gtk_label_new("Name:"));
    gtk_container_add(GTK_CONTAINER(display_box), pwmgr->display_name_label);

    gtk_container_add(GTK_CONTAINER(display_box), gtk_label_new("Username:"));
    gtk_container_add(GTK_CONTAINER(display_box), pwmgr->display_un_label);

    gtk_container_add(GTK_CONTAINER(display_box), gtk_label_new("Password:"));
    gtk_container_add(GTK_CONTAINER(display_box), pwmgr->display_pw_label);

    gtk_container_add(GTK_CONTAINER(display_box),
                      gtk_label_new("Description:"));
    gtk_container_add(GTK_CONTAINER(display_box), pwmgr->display_desc_label);

    GtkWidget *show_pw_btn = gtk_button_new_with_label("Show password");
    g_signal_connect(show_pw_btn, "clicked",
                     G_CALLBACK(decrypt_password_handler), pwmgr);
    gtk_container_add(GTK_CONTAINER(display_box), show_pw_btn);

    gtk_container_add(GTK_CONTAINER(scrolled_window), list_box);
    gtk_box_pack_start(GTK_BOX(bottom_box), scrolled_window, 0, 0, 10);
    gtk_box_pack_start(GTK_BOX(bottom_box), display_box, 0, 0, 10);

    // gtk_window_set_child(GTK_WINDOW(window), box);
    gtk_container_add(GTK_CONTAINER(pwmgr->main_window), main_box);
    gtk_widget_show_all(pwmgr->main_window);
}

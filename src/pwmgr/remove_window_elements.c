#include "pwmgr.h"

void pwmgr_remove_window_elements(GtkWidget *window) {
    GList *list = gtk_container_get_children(GTK_CONTAINER(window));
    if (!list) {
        printf("no children to remove\n");
        return;
    }
    for (GList *temp = list; temp != NULL; temp = temp->next) {
        gtk_container_remove(GTK_CONTAINER(window), GTK_WIDGET(temp->data));
    }
}

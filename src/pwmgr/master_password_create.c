#include "../queries.h"
#include "pwmgr.h"

#include <crypt.h>

void pwmgr_master_password_create_handler(GtkButton *btn, gpointer data) {
    if (!btn) {
    }
    pwmgr_create_master_pw_form *form = (pwmgr_create_master_pw_form *)data;
    if (!form)
        printf("Failed to cast gpointer\n");

    // Reset the error label in case of multiple attempts
    gtk_label_set_text(GTK_LABEL(form->error_label), "");

    const char *text1 = gtk_entry_get_text(GTK_ENTRY(form->entry1));
    const char *text2 = gtk_entry_get_text(GTK_ENTRY(form->entry2));
    if (!text1 || !text2) {
        gtk_label_set_text(GTK_LABEL(form->error_label),
                           "Both passwords must be filled out and match");
        return;
    }

    size_t len = strlen(text1);
    if (len < 8) {
        gtk_label_set_text(GTK_LABEL(form->error_label),
                           "Password isn't long enough, 8 characters minimum");
        return;
    }
    int result = strcmp(text1, text2);
    if (result != 0) {
        gtk_label_set_text(GTK_LABEL(form->error_label),
                           "Passwords don't match");
        return;
    }

    const char *algo = "$y$";
    char *salt = crypt_gensalt_ra(algo, 1, NULL, 0);
    if (!salt) {
        printf("Generating salt failed\n");
    }

    struct crypt_data *temp_data = calloc(1, sizeof(struct crypt_data));
    if (!temp_data)
        printf("failed to allocate memory for hashing password\n");

    char *hashed = crypt_r(text1, salt, temp_data);
    if (!hashed)
        printf("Failed to hash password?\n");

    sqlite3_stmt *stmt;
    result = sqlite3_prepare_v2(form->pwmgr->db, qry_insert_master_pw,
                                strlen(qry_insert_master_pw), &stmt, NULL);
    if (result != SQLITE_OK) {
        printf("%s\n", sqlite3_errmsg(form->pwmgr->db));
        return;
    }

    result = sqlite3_bind_text(stmt, 1, hashed, strlen(hashed), NULL);
    if (result != SQLITE_OK) {
        printf("%s\n", sqlite3_errmsg(form->pwmgr->db));
        return;
    }

    result = sqlite3_bind_text(stmt, 2, salt, strlen(hashed), NULL);
    if (result != SQLITE_OK) {
        printf("%s\n", sqlite3_errmsg(form->pwmgr->db));
        return;
    }

    result = sqlite3_step(stmt);
    if (result != SQLITE_DONE) {
        printf("%s\n", sqlite3_errmsg(form->pwmgr->db));
        return;
    }

    result = sqlite3_finalize(stmt);
    if (result != SQLITE_OK) {
        printf("%s\n", sqlite3_errmsg(form->pwmgr->db));
        return;
    }

    form->pwmgr->master_hash = g_string_new(hashed);
    form->pwmgr->master_salt = g_string_new(salt);

    // DO THIS AT THE END
    free(temp_data);
    pwmgr_unlock(form->pwmgr);
}

void pwmgr_master_password_create(Pwmgr *p) {
    if (!p)
        printf("pwmgr_unlock_handler received a NULL isntance of Pwmgr\n");

    const char *header_text =
        "Welcome to Basic-GTK-Pwmgr"
        "\n\nEnter a password below to begin using"
        "\n\nWarning: This password will be required when opening this "
        "application in the future, losing it will result in all data "
        "being lost.";
    pwmgr_create_master_pw_form *form =
        calloc(1, sizeof(pwmgr_create_master_pw_form));
    if (!form) {
        printf("Failed to allocate memory for pwmgr_create_master_pw_form\n");
    }
    form->pwmgr = p;

    GtkWidget *main_box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 10);
    GtkWidget *header_label = gtk_label_new(header_text);

    GtkWidget *label1 = gtk_label_new("Enter Password");
    GtkWidget *label2 = gtk_label_new("Enter Password again");

    form->entry1 = gtk_entry_new();
    form->entry2 = gtk_entry_new();
    gtk_entry_set_visibility(GTK_ENTRY(form->entry1), 0);
    gtk_entry_set_visibility(GTK_ENTRY(form->entry2), 0);

    GtkWidget *btn = gtk_button_new_with_label("Save");
    form->error_label = gtk_label_new("");

    gtk_container_add(GTK_CONTAINER(main_box), header_label);

    gtk_container_add(GTK_CONTAINER(main_box), label1);
    gtk_container_add(GTK_CONTAINER(main_box), form->entry1);

    gtk_container_add(GTK_CONTAINER(main_box), label2);
    gtk_container_add(GTK_CONTAINER(main_box), form->entry2);

    gtk_container_add(GTK_CONTAINER(main_box), btn);
    gtk_container_add(GTK_CONTAINER(main_box), form->error_label);

    g_signal_connect(btn, "clicked",
                     G_CALLBACK(pwmgr_master_password_create_handler), form);
    gtk_container_add(GTK_CONTAINER(p->main_window), main_box);
    gtk_widget_show_all(p->main_window);
}

#include "pwmgr.h"
#include "src/queries.h"
#include <openssl/conf.h>
#include <openssl/err.h>
#include <openssl/evp.h>

void decrypt_password_handler(GtkButton *btn, gpointer data) {
    if (!btn)
        printf("btn param is NULL\n");
    Pwmgr *pwmgr = (Pwmgr *)data;
    if (!pwmgr) {
        printf("casting gpointer to Pwmgr* failed\n");
        return;
    }

    // Get the selected account name from the listbox
    GtkListBoxRow *row =
        gtk_list_box_get_selected_row(GTK_LIST_BOX(pwmgr->list_box));
    GtkWidget *label = gtk_bin_get_child(GTK_BIN(row));
    const char *account_name = gtk_label_get_text(GTK_LABEL(label));

    sqlite3_stmt *stmt;
    int result =
        sqlite3_prepare_v2(pwmgr->db, qry_get_account_by_name,
                           strlen(qry_get_account_by_name), &stmt, NULL);
    if (result != SQLITE_OK) {
        printf("%s\n", sqlite3_errmsg(pwmgr->db));
    }

    result =
        sqlite3_bind_text(stmt, 1, account_name, strlen(account_name), NULL);

    if (result != SQLITE_OK) {
        printf("%s\n", sqlite3_errmsg(pwmgr->db));
    }

    result = sqlite3_step(stmt);
    if (result != SQLITE_ROW) {
        printf("%s\n", sqlite3_errmsg(pwmgr->db));
    }

    const char *encrypted_pw = (char *)sqlite3_column_text(stmt, 3);
    const char *iv = (char *)sqlite3_column_text(stmt, 5);
    printf("pw = %s\n", encrypted_pw);
    printf("iv = %s\n", iv);

    // base64 decode the encrypted password
    char base64_decoded[1024] = {0};
    result =
        EVP_DecodeBlock((unsigned char *)base64_decoded,
                        (unsigned char *)encrypted_pw, strlen(encrypted_pw));
    printf("result of base64 decode = %d\n", result);

    char decrypted_pw[1024] = {0};
    result = decrypt((unsigned char *)base64_decoded, strlen(base64_decoded),
                     (unsigned char *)pwmgr->master_key->str,
                     (unsigned char *)iv, (unsigned char *)decrypted_pw);

    printf("result of decryption = %d\n", result);
    printf("Decrypted pw = |%s|\n", decrypted_pw);
    sqlite3_finalize(stmt);
    gtk_label_set_text(GTK_LABEL(pwmgr->display_pw_label), decrypted_pw);
}

int encrypt(unsigned char *plaintext, int plaintext_len, unsigned char *key,
            unsigned char *iv, unsigned char *ciphertext) {
    EVP_CIPHER_CTX *ctx;

    int len;

    int ciphertext_len;

    /* Create and initialise the context */
    ctx = EVP_CIPHER_CTX_new();
    if (!ctx)
        return -1;

    /*
     * Initialise the encryption operation. IMPORTANT - ensure you use a key
     * and IV size appropriate for your cipher
     * In this example we are using 256 bit AES (i.e. a 256 bit key). The
     * IV size for *most* modes is the same as the block size. For AES this
     * is 128 bits
     */
    if (1 != EVP_EncryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv))
        return -1;

    /*
     * Provide the message to be encrypted, and obtain the encrypted output.
     * EVP_EncryptUpdate can be called multiple times if necessary
     */
    if (1 != EVP_EncryptUpdate(ctx, ciphertext, &len, plaintext, plaintext_len))
        return -1;

    ciphertext_len = len;

    /*
     * Finalise the encryption. Further ciphertext bytes may be written at
     * this stage.
     */
    if (1 != EVP_EncryptFinal_ex(ctx, ciphertext + len, &len))
        return -1;

    ciphertext_len += len;

    /* Clean up */
    EVP_CIPHER_CTX_free(ctx);

    return ciphertext_len;
}

int decrypt(unsigned char *ciphertext, int ciphertext_len, unsigned char *key,
            unsigned char *iv, unsigned char *plaintext) {
    EVP_CIPHER_CTX *ctx;

    int len;

    int plaintext_len;

    /* Create and initialise the context */
    if (!(ctx = EVP_CIPHER_CTX_new()))
        return -1;

    /*
     * Initialise the decryption operation. IMPORTANT - ensure you use a key
     * and IV size appropriate for your cipher
     * In this example we are using 256 bit AES (i.e. a 256 bit key). The
     * IV size for *most* modes is the same as the block size. For AES this
     * is 128 bits
     */
    if (1 != EVP_DecryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv))
        return -1;

    /*
     * Provide the message to be decrypted, and obtain the plaintext output.
     * EVP_DecryptUpdate can be called multiple times if necessary.
     */
    if (1 !=
        EVP_DecryptUpdate(ctx, plaintext, &len, ciphertext, ciphertext_len))
        return -1;

    plaintext_len = len;

    /*
     * Finalise the decryption. Further plaintext bytes may be written at
     * this stage.
     */
    if (1 != EVP_DecryptFinal_ex(ctx, plaintext + len, &len))
        return -1;

    plaintext_len += len;

    /* Clean up */
    EVP_CIPHER_CTX_free(ctx);

    return plaintext_len;
}

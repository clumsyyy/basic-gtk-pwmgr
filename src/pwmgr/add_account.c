#include "../queries.h"
#include "pwmgr.h"

void pwmgr_add_account(GtkButton *btn, gpointer data) {
    if (!btn) {
    }
    Pwmgr *pwmgr = (Pwmgr *)data;
    if (!pwmgr)
        printf("Failed to cast gpointer\n");

    pwmgr_remove_window_elements(pwmgr->main_window);
    pwmgr_account_form *form = calloc(1, sizeof(pwmgr_account_form));
    if (!form)
        printf("Failed to allocate memory for form struct\n");

    form->pwmgr = pwmgr;

    GtkWidget *box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 10);

    GtkWidget *name_label = gtk_label_new("Name");
    form->name_entry = gtk_entry_new();
    gtk_box_pack_start(GTK_BOX(box), name_label, 0, 0, 10);
    gtk_box_pack_start(GTK_BOX(box), form->name_entry, 0, 0, 10);

    GtkWidget *un_label = gtk_label_new("Username");
    form->un_entry = gtk_entry_new();
    gtk_box_pack_start(GTK_BOX(box), un_label, 0, 0, 10);
    gtk_box_pack_start(GTK_BOX(box), form->un_entry, 0, 0, 10);

    GtkWidget *pw_label = gtk_label_new("Password");
    form->pw_entry = gtk_entry_new();
    gtk_entry_set_visibility(GTK_ENTRY(form->pw_entry), 0);
    gtk_box_pack_start(GTK_BOX(box), pw_label, 0, 0, 10);
    gtk_box_pack_start(GTK_BOX(box), form->pw_entry, 0, 0, 10);

    GtkWidget *desc_label = gtk_label_new("Description");
    form->desc_entry = gtk_entry_new();
    gtk_box_pack_start(GTK_BOX(box), desc_label, 0, 0, 10);
    gtk_box_pack_start(GTK_BOX(box), form->desc_entry, 0, 0, 10);

    GtkWidget *save_btn = gtk_button_new_with_label("Save");
    g_signal_connect(save_btn, "clicked", G_CALLBACK(pwmgr_add_account_handler),
                     form);
    gtk_box_pack_start(GTK_BOX(box), save_btn, 0, 0, 10);

    GtkWidget *cancel_btn = gtk_button_new_with_label("Cancel");
    g_signal_connect(cancel_btn, "clicked",
                     G_CALLBACK(pwmgr_add_account_cancel), pwmgr);
    gtk_box_pack_start(GTK_BOX(box), cancel_btn, 0, 0, 10);

    gtk_container_add(GTK_CONTAINER(pwmgr->main_window), box);
    // gtk_window_present(GTK_WINDOW(popup));
    gtk_widget_show_all(pwmgr->main_window);
}

void pwmgr_add_account_handler(GtkButton *btn, gpointer data) {
    if (!btn || !data)
        printf("null params\n");

    pwmgr_account_form *form = (pwmgr_account_form *)data;
    if (!form)
        printf("failed to cast gpointer\n");

    const char *un = gtk_entry_get_text(GTK_ENTRY(form->un_entry));
    const char *name = gtk_entry_get_text(GTK_ENTRY(form->name_entry));
    const char *pw = gtk_entry_get_text(GTK_ENTRY(form->pw_entry));
    const char *desc = gtk_entry_get_text(GTK_ENTRY(form->desc_entry));

    sqlite3_stmt *stmt;
    int result = sqlite3_prepare_v2(form->pwmgr->db, qry_insert_account,
                                    strlen(qry_insert_account), &stmt, NULL);
    if (result != SQLITE_OK) {
        printf("%s: %d\n", sqlite3_errmsg(form->pwmgr->db), result);
    }

    result = sqlite3_bind_text(stmt, 1, name, strlen(name), NULL);
    if (result != SQLITE_OK) {
        printf("%s: %d\n", sqlite3_errmsg(form->pwmgr->db), result);
    }

    result = sqlite3_bind_text(stmt, 2, un, strlen(un), NULL);
    if (result != SQLITE_OK) {
        printf("%s: %d\n", sqlite3_errmsg(form->pwmgr->db), result);
    }

    result = sqlite3_bind_text(stmt, 4, desc, strlen(desc), NULL);
    if (result != SQLITE_OK) {
        printf("%s: %d\n", sqlite3_errmsg(form->pwmgr->db), result);
    }

    // Nothing has failed yet so generate a 128 bit random IV
    // to be used for password encryption
    char iv[129] = {0};
    for (int i = 0; i < 128; i++) {
        iv[i] = (rand() % (126 - 33 + 1)) + 33;
    }

    // encrypt the password string
    char encrypted_pw[1024] = {0};
    int encrypted_len =
        encrypt((unsigned char *)pw, strlen(pw),
                (unsigned char *)form->pwmgr->master_key->str,
                (unsigned char *)iv, (unsigned char *)encrypted_pw);
    if (encrypted_len <= 0) {
        printf("encryption failed?\n");
        return;
    }

    // now base64 encode the password so it shows up correctly when
    // displayed
    unsigned char newbuf[1000] = {0};
    result = 0;
    result =
        EVP_EncodeBlock(newbuf, (unsigned char *)encrypted_pw, encrypted_len);

    result = sqlite3_bind_text(stmt, 3, (char *)newbuf, strlen((char *)newbuf),
                               NULL);
    if (result != SQLITE_OK) {
        printf("%s: %d\n", sqlite3_errmsg(form->pwmgr->db), result);
    }

    result = sqlite3_bind_text(stmt, 5, iv, strlen(iv), NULL);
    if (result != SQLITE_OK) {
        printf("%s: %d\n", sqlite3_errmsg(form->pwmgr->db), result);
    }

    result = sqlite3_step(stmt);
    if (result != SQLITE_DONE) {
        printf("%s: %d\n", sqlite3_errmsg(form->pwmgr->db), result);
    }

    result = sqlite3_finalize(stmt);
    if (result != SQLITE_OK) {
        printf("%s: %d\n", sqlite3_errmsg(form->pwmgr->db), result);
    }

    pwmgr_show_main(form->pwmgr);
}

void pwmgr_add_account_cancel(GtkButton *btn, gpointer data) {
    if (!btn) {
    }
    Pwmgr *pwmgr = (Pwmgr *)data;
    if (!pwmgr)
        printf("casting gpointer failed\n");

    pwmgr_show_main(pwmgr);
}

#include "pwmgr.h"

#include <crypt.h>
#include <gtk/gtk.h>
#include <openssl/conf.h>
#include <openssl/err.h>
#include <openssl/evp.h>

void pwmgr_unlock(Pwmgr *p) {
    if (!p)
        printf("Pwmgr instance is null?\n");
    pwmgr_remove_window_elements(p->main_window);

    GtkWidget *lbl =
        gtk_label_new("Enter your password to unlock the application");
    GtkWidget *entry = gtk_entry_new();
    gtk_entry_set_visibility(GTK_ENTRY(entry), 0);

    GtkWidget *error_lbl = gtk_label_new("");

    GtkWidget *btn = gtk_button_new_with_label("Unlock");
    pwmgr_unlock_master_pw_form *form =
        calloc(1, sizeof(pwmgr_unlock_master_pw_form));
    if (!form) {
        printf("calloc failed to allocate memory for "
               "pwmgr_unlock_master_pw_form\n");
    }
    form->entry = entry;
    form->error_label = error_lbl;
    form->pwmgr = p;

    g_signal_connect(btn, "clicked", G_CALLBACK(pwmgr_unlock_handler), form);

    GtkWidget *box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 10);
    gtk_box_pack_start(GTK_BOX(box), lbl, 0, 0, 10);
    gtk_box_pack_start(GTK_BOX(box), entry, 0, 0, 10);
    gtk_box_pack_start(GTK_BOX(box), error_lbl, 0, 0, 10);
    gtk_box_pack_start(GTK_BOX(box), btn, 0, 0, 10);
    gtk_container_add(GTK_CONTAINER(p->main_window), box);
    gtk_widget_show_all(p->main_window);
}

void pwmgr_unlock_handler(GtkButton *btn, gpointer data) {
    printf("pwmgr_unlock_handler\n");
    if (!btn) {
    } // dummy line

    pwmgr_unlock_master_pw_form *form = (pwmgr_unlock_master_pw_form *)data;
    if (!form)
        printf("Failed to cast gpointer in pwmgr_unlock_handler\n");

    const char *text = gtk_entry_get_text(GTK_ENTRY(form->entry));
    if (!text) {
        gtk_label_set_text(GTK_LABEL(form->error_label),
                           "Failed to get password from form");
        return;
    }

    struct crypt_data *temp_data = calloc(1, sizeof(struct crypt_data));
    if (!temp_data) {
        printf("Failed to allocate memory to comparing entered password "
               "against saved hash\n");
        return;
    }

    char *hashed = crypt_r(text, form->pwmgr->master_salt->str, temp_data);
    if (!hashed)
        printf("hashed is null\n");

    printf("Saved hash=%s, saved salt=%s, new hash=%s\n",
           form->pwmgr->master_hash->str, form->pwmgr->master_salt->str, text);

    int result = strcmp(hashed, form->pwmgr->master_hash->str);
    if (result != 0) {
        printf("result = %d\n", result);
        gtk_label_set_text(GTK_LABEL(form->error_label), "Incorrect password");
        return;
    } else {
        // the password is correct, sha512 hash it then b64 encode it
        // to be used as the "key" for encrypting account passwords
        EVP_MD_CTX *ctx = EVP_MD_CTX_create();
        if (!ctx)
            printf("failed to create EVP_MD_CTX\n");

        int result = EVP_MD_CTX_init(ctx);
        printf("result = %d after EVP_MD_CTX_init() \n", result);

        result = EVP_DigestInit_ex(ctx, EVP_sha512(), NULL);
        printf("result = %d after EVP_DigestInit_ex() \n", result);

        result = EVP_DigestUpdate(ctx, text, strlen((char *)text));
        printf("result = %d after EVP_DigestUpdate() \n", result);

        unsigned char buffer[EVP_MAX_MD_SIZE + 1] = {0};
        result = EVP_DigestFinal_ex(ctx, buffer, NULL);

        // printf("result = %d, Buffer = |%s|, len = %lu\n", result,
        //(char *)buffer, strlen((char *)buffer));

        EVP_MD_CTX_destroy(ctx);

        unsigned char newbuf[1000] = {0};
        result = 0;
        result = EVP_EncodeBlock(newbuf, buffer, strlen((char *)buffer));
        printf("result = %d, sha512->b64 = %s, len = %lu\n", result,
               (char *)newbuf, strlen((char *)newbuf));
        form->pwmgr->master_key = g_string_new((char *)newbuf);
        printf("%s, len = %lu\n", form->pwmgr->master_key->str,
               form->pwmgr->master_key->len);

        pwmgr_show_main(form->pwmgr);
    }

    // DO THIS AT THE END
    // free(temp_data);
}

#include "pwmgr.h"
#include "../queries.h"
#include "../settings.h"

#include <gtk/gtk.h>
#include <sqlite3.h>
#include <sys/stat.h>

static GString *find_data_dir(void);
static void setup_db(Pwmgr *);
static void handle_db_schema(sqlite3 *);
static void activate_signal(GtkApplication *, gpointer);
static void get_master_pw(Pwmgr *);

Pwmgr *pwmgr_init(void) {
    Pwmgr *p = (Pwmgr *)calloc(1, sizeof(Pwmgr));
    p->data_dir = find_data_dir();

    setup_db(p);
    p->app = gtk_application_new(pwmgr_gtk_application_name,
                                 G_APPLICATION_DEFAULT_FLAGS);
    g_signal_connect(p->app, "activate", G_CALLBACK(activate_signal), p);

    get_master_pw(p);

    return p;
}

static void get_master_pw(Pwmgr *p) {
    printf("get_master_pw called\n");
    sqlite3_stmt *stmt;
    int result = sqlite3_prepare_v2(p->db, qry_select_master_pw,
                                    strlen(qry_select_master_pw), &stmt, NULL);
    if (result != SQLITE_OK) {
        printf("master password query failed, sqlite3 errmsg: %s\n",
               sqlite3_errmsg(p->db));
        exit(-1);
    }

    result = sqlite3_step(stmt);
    if (result == SQLITE_ROW) {
        const unsigned char *hash = sqlite3_column_text(stmt, 0);
        if (!hash)
            printf("Queried hash is empty\n");
        p->master_hash = g_string_new((char *)hash);

        const unsigned char *salt = sqlite3_column_text(stmt, 1);
        if (!salt)
            printf("Queried salt is empty\n");
        p->master_salt = g_string_new((char *)salt);

        printf("hash = %s, salt = %s\n", p->master_hash->str,
               p->master_salt->str);
    } else {
        printf("sqlite3 result = %d, errmsg = %s\n", result,
               sqlite3_errmsg(p->db));
    }

    result = sqlite3_finalize(stmt);
    if (result != SQLITE_OK)
        printf("sqlite3 result = %d, errmsg = %s\n", result,
               sqlite3_errmsg(p->db));
}

static void setup_db(Pwmgr *p) {
    // Create a string to hold the database path
    p->db_path = g_string_new(p->data_dir->str);
    g_string_append(p->db_path, db_file);

    // attempt to open the database
    sqlite3 *db;
    int result = sqlite3_open_v2(p->db_path->str, &db,
                                 SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, 0);
    if (result != SQLITE_OK) {
        printf("Failed to open %s, unable to continue, sqlite error: %s\n",
               p->db_path->str, sqlite3_errmsg(db));
        exit(-1);
    }

    // Handle database startup and health checks
    handle_db_schema(db);
    p->db = db;
}

void handle_db_schema(sqlite3 *db) {
    if (!db) {
        printf("handle_db_schema received a NULL pointer database\n");
        exit(-1);
    }

    char *err;
    int result = sqlite3_exec(db, qry_schema, NULL, NULL, &err);
    if (result != SQLITE_OK) {
        printf("sqlite result: %d, sqlite_errmsg: %s, sqlite buffer: %s\n",
               result, sqlite3_errmsg(db), err);
        exit(-1);
    }
}

static GString *find_data_dir(void) {
    // Find  $HOME, if not found crash, unable to start because unable to locate
    // directory where to read/write data
    char *home = getenv("HOME");
    if (!home) {
        printf("Failed to find directory to write data, $HOME is unset "
               "somehow?\n");
        exit(-1);
    }

    GString *path = g_string_new(home);
    g_string_append(path, default_data_dir);

    // Confirm $HOME/.local/share exists
    struct stat st;
    int result = stat(path->str, &st);
    if (result != 0) {
        printf("Failed to find %s, creating new...\n", path->str);
        result = mkdir(path->str, S_IRWXU);
        if (result != 0) {
            printf("Failed to create %s, exiting\n", path->str);
            exit(-1);
        }
    }
    return path;
}

static void activate_signal(GtkApplication *gtkApp, gpointer data) {
    Pwmgr *p = (Pwmgr *)data;
    if (!p)
        printf("Failed to cast gpointer on activation\n");

    GtkWidget *window = gtk_application_window_new(gtkApp);
    gtk_window_set_title(GTK_WINDOW(window), "Basic-GTK-Pwmgr");
    gtk_window_set_default_size(GTK_WINDOW(window), 800, 400);
    p->main_window = window;

    if (!p->master_hash || !p->master_salt) {
        printf("Saved hash and salt not found, prompting for new\n");
        pwmgr_master_password_create(p);
    } else {
        pwmgr_unlock(p);
    }
}

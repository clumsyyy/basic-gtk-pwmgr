#include "pwmgr.h"
#include "account.hpp"
#include "queries.h"
#include "utils.hpp"

#include <crypt.h>

Pwmgr::Pwmgr(GtkApplication *app, std::string dir, sqlite3 *db) {
    std::cout << "pwmgr constructor called\n";
    if (!app) {
        std::cout << "GtkApplication* param is null, failing\n";
        exit(-1);
    }
    if (!db) {
        std::cout << "db pointer is null in pwmgr constructor\n";
    }

    this->app = app;
    this->dataDir = dir;
    this->db = db;

    std::cout << "made it past setting app/dir/db\n";
    // query the db for the master password
    sqlite3_stmt *stmt;
    int result = sqlite3_prepare_v2(db, qry_select_master_pw.c_str(),
                                    qry_select_master_pw.size(), &stmt, NULL);
    std::cout << "made it past sqlite3_stmt being used in sqlite3_prepare_v2\n";
    if (result != SQLITE_OK) {
        std::cout << "master password query failed, sqlite3 errmsg: "
                  << sqlite3_errmsg(db);
        sqlite3_free(db);
        exit(-1);
    }

    result = sqlite3_step(stmt);
    if (result == SQLITE_ROW) {
        const unsigned char *hash = sqlite3_column_text(stmt, 0);
        if (!hash)
            std::cout << "Queried hash is empty\n";
        this->masterPasswordHash = std::string((char *)hash);

        const unsigned char *salt = sqlite3_column_text(stmt, 1);
        if (!salt)
            std::cout << "Queried salt is empty\n";
        this->masterPasswordSalt = std::string((char *)salt);

        std::cout << "hash = " << hash << ", salt = " << salt << std::endl;
    } else {
        std::cout << "sqlite3 result = " << result
                  << ", errmsg = " << sqlite3_errmsg(db) << std::endl;
    }

    result = sqlite3_finalize(stmt);
    if (result != SQLITE_OK)
        std::cout << "sqlite3 result = " << result << ", " << sqlite3_errmsg(db)
                  << std::endl;
}

/*
 * Pwmgr::activateSignalHandler is the main "activate" signal function
 * for the GtkApplication *ptr that gets created at startup. This handles
 * setting up the entire GUI and registering initial functions
 */
void Pwmgr::activateSignalHandler(GtkApplication *gtkApp, gpointer data) {
    Pwmgr *p = (Pwmgr *)data;
    if (!p)
        std::cout << "unable to cast gpointer to pwmgr*\n";
    else
        std::cout << "casting gpointer to Pwmgr* worked\n";

    // Create the main window and give it to Pwmgr
    GtkWidget *window = gtk_application_window_new(gtkApp);
    gtk_window_set_title(GTK_WINDOW(window), "Basic-GTK-Pwmgr");
    gtk_window_set_default_size(GTK_WINDOW(window), 800, 400);
    p->mainWindow = window;

    // Check if we have ran before, should have a masterPasswordHash and
    // masterPasswordSalt fields populated if the user has started the
    // application and set a password
    if (p->masterPasswordHash.size() > 0 && p->masterPasswordSalt.size() > 0) {
        // Have both fields that should be correct, proceed with
        // showing unlock window
        Pwmgr::normalStartup(p);
    } else {
        // No master password hash/salt are present in the DB, prompt the
        // user to create new ones
        Pwmgr::firstStartup(p);
    }
}

void Pwmgr::showAddAccountForm(GtkButton *btn, gpointer data) {
    if (!btn || !data) {
    }
    Pwmgr *p = (Pwmgr *)data;
    if (!p)
        std::cout << "unable to cast gpointer to pwmgr*\n";

    GtkWidget *popup = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(popup), "Add an Account");
    gtk_window_set_default_size(GTK_WINDOW(popup), 600, 400);

    GtkWidget *box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 10);

    GtkWidget *nameLbl = gtk_label_new("Name");
    GtkWidget *nameEntry = gtk_entry_new();
    gtk_box_pack_start(GTK_BOX(box), nameLbl, 0, 0, 10);
    gtk_box_pack_start(GTK_BOX(box), nameEntry, 0, 0, 10);

    GtkWidget *usernameLbl = gtk_label_new("Username");
    GtkWidget *usernameEntry = gtk_entry_new();
    gtk_box_pack_start(GTK_BOX(box), usernameLbl, 0, 0, 10);
    gtk_box_pack_start(GTK_BOX(box), usernameEntry, 0, 0, 10);

    GtkWidget *pwLabel = gtk_label_new("Password");
    GtkWidget *pwEntry = gtk_entry_new();
    gtk_entry_set_visibility(GTK_ENTRY(pwEntry), 0);
    gtk_box_pack_start(GTK_BOX(box), pwLabel, 0, 0, 10);
    gtk_box_pack_start(GTK_BOX(box), pwEntry, 0, 0, 10);

    GtkWidget *descLbl = gtk_label_new("Description");
    GtkWidget *descEntry = gtk_entry_new();
    gtk_box_pack_start(GTK_BOX(box), descLbl, 0, 0, 10);
    gtk_box_pack_start(GTK_BOX(box), descEntry, 0, 0, 10);

    AccountForm *form =
        new AccountForm(p, popup, nameEntry, usernameEntry, pwEntry, descEntry);

    GtkWidget *submitBtn = gtk_button_new_with_label("Submit");
    g_signal_connect(submitBtn, "clicked",
                     G_CALLBACK(Pwmgr::submitAddAccountForm), form);
    gtk_box_pack_start(GTK_BOX(box), submitBtn, 0, 0, 10);

    gtk_container_add(GTK_CONTAINER(popup), box);
    // gtk_window_present(GTK_WINDOW(popup));
    gtk_widget_show_all(popup);
}

void Pwmgr::submitAddAccountForm(GtkButton *btn, gpointer data) {
    if (!btn) {
    }
    AccountForm *form = (AccountForm *)data;
    if (!form)
        std::cout << "unable to cast gpointer to pwmgr*\n";

    GtkEntryBuffer *nameBuf = gtk_entry_get_buffer(GTK_ENTRY(form->nameEntry));
    if (!nameBuf)
        std::cout << __func__ << " failed to get name entry buffer\n";

    GtkEntryBuffer *unBuf = gtk_entry_get_buffer(GTK_ENTRY(form->unEntry));
    if (!nameBuf)
        std::cout << __func__ << " failed to get username entry buffer\n";

    GtkEntryBuffer *pwBuf = gtk_entry_get_buffer(GTK_ENTRY(form->pwEntry));
    if (!nameBuf)
        std::cout << __func__ << " failed to get password entry buffer\n";

    GtkEntryBuffer *descBuf = gtk_entry_get_buffer(GTK_ENTRY(form->descEntry));
    if (!nameBuf)
        std::cout << __func__ << " failed to get description entry buffer\n";

    std::string name = gtk_entry_buffer_get_text(nameBuf);
    std::string un = gtk_entry_buffer_get_text(unBuf);
    std::string pw = gtk_entry_buffer_get_text(pwBuf);
    std::string desc = gtk_entry_buffer_get_text(descBuf);

    sqlite3 *db = form->mgr->db;
    if (!db) {
        std::cout << "Pwmgr::db is NULL\n";
        exit(-1);
    }

    sqlite3_stmt *stmt;
    int result = sqlite3_prepare_v2(db, qry_insert_account.c_str(),
                                    qry_insert_account.size(), &stmt, NULL);
    if (result != SQLITE_OK) {
        std::cout << "result = " << result
                  << ", sqlite error: " << sqlite3_errmsg(db) << std::endl;
        result = sqlite3_finalize(stmt);
        if (result != SQLITE_OK) {
            std::cout << "result = " << result
                      << ", sqlite error: " << sqlite3_errmsg(db) << std::endl;
        }
    }
    // bind the name text to the statement
    result = sqlite3_bind_text(stmt, 1, name.c_str(), name.size(), NULL);
    if (result != SQLITE_OK) {
        std::cout << "result = " << result
                  << ", sqlite error: " << sqlite3_errmsg(db) << std::endl;
    }
    result = sqlite3_bind_text(stmt, 2, un.c_str(), un.size(), NULL);
    if (result != SQLITE_OK) {
        std::cout << "result = " << result
                  << ", sqlite error: " << sqlite3_errmsg(db) << std::endl;
    }
    result = sqlite3_bind_text(stmt, 3, pw.c_str(), pw.size(), NULL);
    if (result != SQLITE_OK) {
        std::cout << "result = " << result
                  << ", sqlite error: " << sqlite3_errmsg(db) << std::endl;
    }
    result = sqlite3_bind_text(stmt, 4, desc.c_str(), desc.size(), NULL);
    if (result != SQLITE_OK) {
        std::cout << "result = " << result
                  << ", sqlite error: " << sqlite3_errmsg(db) << std::endl;
    }

    result = sqlite3_step(stmt);
    if (result != SQLITE_DONE) {
        std::cout << "insert row: sqlite3_step result = " << result
                  << ", sqlite error: " << sqlite3_errmsg(db) << std::endl;
    }

    result = sqlite3_finalize(stmt);
    if (result != SQLITE_OK) {
        std::cout << "failed to finalize sqlite3_stmt, sqlite error: "
                  << sqlite3_errmsg(db) << std::endl;
    }
    gtk_window_close(GTK_WINDOW(form->popupWindow));
    form->mgr->refreshListHandler(NULL, form->mgr);
}

void Pwmgr::refreshListHandler(GtkButton *btn, gpointer data) {
    std::cout << "Pwmgr::refreshListHandler called\n";
    if (!btn) {
    }
    if (!data) {
        std::cout << "refreshListHandler has NULL data param\n";
    }

    Pwmgr *p = (Pwmgr *)data;
    if (!p) {
        std::cout << "refreshListHandler failed to cast gpointer\n";
    }

    GList *children = gtk_container_get_children(GTK_CONTAINER(p->listBox));
    for (GList *temp = children; temp != NULL; temp = temp->next) {
        if (GTK_IS_WIDGET(temp->data)) {
            std::cout << "is widget\n";
            GtkWidget *w = GTK_WIDGET(temp->data);
            if (GTK_IS_LIST_BOX_ROW(w)) {
                gtk_container_remove(GTK_CONTAINER(p->listBox), w);
            }
        }
    }

    std::vector<Account> accts = getAccounts(p->db);
    for (size_t i = 0; i < accts.size(); i++) {
        Account a = accts.at(i);
        GtkWidget *row = gtk_list_box_row_new();
        GtkWidget *lbl = gtk_label_new(a.name.c_str());
        gtk_container_add(GTK_CONTAINER(row), lbl);
        gtk_container_add(GTK_CONTAINER(p->listBox), row);
    }
    gtk_widget_show_all(p->listBox);
}

void Pwmgr::delBtnHandler(GtkButton *btn, gpointer data) {
    if (!data) {
    }
    if (!btn) {
    }
    Pwmgr *p = (Pwmgr *)data;
    if (!p)
        std::cout << "failed to cast gpointer\n";

    GtkListBoxRow *row =
        gtk_list_box_get_selected_row(GTK_LIST_BOX(p->listBox));
    GtkWidget *lbl = gtk_bin_get_child(GTK_BIN(row));

    std::string text = std::string(gtk_label_get_text(GTK_LABEL(lbl)));
    gtk_container_remove(GTK_CONTAINER(p->listBox), GTK_WIDGET(row));
    sqlite3_stmt *stmt;
    int result = sqlite3_prepare_v2(p->db, qry_delete_account.c_str(),
                                    qry_delete_account.size(), &stmt, NULL);
    if (result != SQLITE_OK) {
        std::cout << "result = " << result
                  << ", sqlite error: " << sqlite3_errmsg(p->db) << std::endl;
    }

    result = sqlite3_bind_text(stmt, 1, text.c_str(), text.size(), NULL);
    if (result != SQLITE_OK) {
        std::cout << "result = " << result
                  << ", sqlite error: " << sqlite3_errmsg(p->db) << std::endl;
    }
    result = sqlite3_step(stmt);
    if (result != SQLITE_DONE) {
        std::cout << "result = " << result
                  << ", sqlite error: " << sqlite3_errmsg(p->db) << std::endl;
    }
}

void Pwmgr::normalStartup(Pwmgr *p) {
    if (!p) {
        std::cout << "normalStartup Pwmgr is null\n";
        exit(-1);
    }
    std::cout << "Normal startup()\n";
    GtkWidget *mainBox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 10);
    GtkWidget *topBox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 10);
    GtkWidget *bottomBox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 10);
    gtk_box_pack_start(GTK_BOX(mainBox), topBox, 0, 0, 10);
    gtk_box_pack_start(GTK_BOX(mainBox), bottomBox, 0, 0, 10);

    GtkWidget *addBtn = gtk_button_new_with_label("Add");
    g_signal_connect(addBtn, "clicked", G_CALLBACK(Pwmgr::showAddAccountForm),
                     p);

    GtkWidget *delBtn = gtk_button_new_with_label("Delete");
    GtkWidget *refreshBtn = gtk_button_new_with_label("Refresh");
    gtk_box_pack_start(GTK_BOX(topBox), addBtn, 0, 0, 10);
    gtk_box_pack_start(GTK_BOX(topBox), delBtn, 0, 0, 10);
    gtk_box_pack_start(GTK_BOX(topBox), refreshBtn, 0, 0, 10);

    // Create the bottom box stuff

    // Create displayBox first

    GtkWidget *scrolledWindow = gtk_scrolled_window_new(NULL, NULL);
    gtk_scrolled_window_set_min_content_height(
        GTK_SCROLLED_WINDOW(scrolledWindow), 400);
    gtk_scrolled_window_set_min_content_width(
        GTK_SCROLLED_WINDOW(scrolledWindow), 200);
    p->scrolledWindow = scrolledWindow;

    GtkWidget *listBox = gtk_list_box_new();
    p->listBox = listBox;
    gtk_list_box_set_selection_mode(GTK_LIST_BOX(listBox),
                                    GTK_SELECTION_SINGLE);
    std::vector<Account> accts = getAccounts(p->db);
    for (size_t i = 0; i < accts.size(); i++) {
        Account a = accts.at(i);
        GtkWidget *row = gtk_list_box_row_new();
        GtkWidget *lbl = gtk_label_new(a.name.c_str());
        gtk_container_add(GTK_CONTAINER(row), lbl);
        gtk_container_add(GTK_CONTAINER(listBox), row);
    }

    // now that listbox is created add the refreshBtn and delBtn handlers
    g_signal_connect(delBtn, "clicked", G_CALLBACK(Pwmgr::delBtnHandler), p);
    g_signal_connect(refreshBtn, "clicked",
                     G_CALLBACK(Pwmgr::refreshListHandler), p);

    gtk_container_add(GTK_CONTAINER(scrolledWindow), listBox);
    gtk_box_pack_start(GTK_BOX(bottomBox), scrolledWindow, 0, 0, 10);

    // gtk_window_set_child(GTK_WINDOW(window), box);
    gtk_container_add(GTK_CONTAINER(p->mainWindow), mainBox);
    gtk_widget_show_all(p->mainWindow);
}

void Pwmgr::firstStartup(Pwmgr *p) {
    if (!p) {
        std::cout << "firstStartup Pwmgr is null\n";
        exit(-1);
    }
    std::string text =
        "Welcome to Basic-GTK-Pwmgr"
        "\n\nEnter a password below to begin using"
        "\n\nWarning: This password will be required when opening this "
        "application in the future, losing it will result in all data "
        "being lost.";

    GtkWidget *mainBox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 10);
    GtkWidget *headerLabel = gtk_label_new(text.c_str());

    GtkWidget *label1 = gtk_label_new("Enter Password");
    GtkWidget *label2 = gtk_label_new("Enter Password again");

    GtkWidget *entry1 = gtk_entry_new();
    GtkWidget *entry2 = gtk_entry_new();
    gtk_entry_set_visibility(GTK_ENTRY(entry1), false);
    gtk_entry_set_visibility(GTK_ENTRY(entry2), false);

    GtkWidget *btn = gtk_button_new_with_label("Save");
    GtkWidget *errLbl = gtk_label_new("");

    gtk_container_add(GTK_CONTAINER(mainBox), headerLabel);

    gtk_container_add(GTK_CONTAINER(mainBox), label1);
    gtk_container_add(GTK_CONTAINER(mainBox), entry1);

    gtk_container_add(GTK_CONTAINER(mainBox), label2);
    gtk_container_add(GTK_CONTAINER(mainBox), entry2);

    gtk_container_add(GTK_CONTAINER(mainBox), btn);
    gtk_container_add(GTK_CONTAINER(mainBox), errLbl);

    FirstPasswordForm *form = new FirstPasswordForm(p, entry1, entry2, errLbl);
    g_signal_connect(btn, "clicked",
                     G_CALLBACK(Pwmgr::firstPasswordSaveHandler), form);

    gtk_container_add(GTK_CONTAINER(p->mainWindow), mainBox);
    gtk_widget_show_all(p->mainWindow);
}

void Pwmgr::firstPasswordSaveHandler(GtkButton *btn, gpointer data) {
    if (!btn || !data)
        std::cout << "Params are null\n";

    FirstPasswordForm *form = (FirstPasswordForm *)data;
    if (!form) {
        std::cout << "failed to convert gpointer to FirstPasswordForm pointer"
                  << std::endl;
    }

    gtk_label_set_text(GTK_LABEL(form->error_label), "");

    Pwmgr *p = form->pwmgr;
    if (!p)
        std::cout << "Failed to convert gpointer to usable\n";

    std::string pw1 = gtk_entry_get_text(GTK_ENTRY(form->entry1));
    std::string pw2 = gtk_entry_get_text(GTK_ENTRY(form->entry2));
    if (pw1 != pw2) {
        gtk_label_set_text(GTK_LABEL(form->error_label),
                           "Passwords do not match");
        return;
    }

    if (pw1.size() < 8) {
        gtk_label_set_text(GTK_LABEL(form->error_label),
                           "Password is to short");
        return;
    }

    // hash password
    std::string algo = "$y$";

    char *salt = crypt_gensalt_ra(algo.c_str(), 1, NULL, 0);
    if (!salt) {
        std::cout << "Failed to generate salt using " << algo << std::endl;
    }

    struct crypt_data mydata;
    memset(&mydata, 1, sizeof(struct crypt_data));

    char *result = crypt_r(pw1.c_str(), salt, &mydata);
    if (!result) {
        printf("result is null\n");
    }
    std::cout << "hash = " << result << std::endl;

    // save password

    // wipe window
    GList *children = gtk_container_get_children(GTK_CONTAINER(p->mainWindow));
    for (GList *temp = children; temp != NULL; temp = temp->next) {
        gtk_container_remove(GTK_CONTAINER(p->mainWindow),
                             GTK_WIDGET(temp->data));
    }

    // delete the class argument
    delete form;

    // call main Pwmgr->normalStartup(p)
    Pwmgr::normalStartup(p);
}

#include <gtk/gtk.h>
#include <sys/stat.h>

#include "pwmgr/pwmgr.h"

/* GLOBALS DEFAULTS - TODO: move to separate header file? */
/* END GLOBALS */

int main(void) {
    // seed rngesus
    srand(time(0));

    // grab pid, useful for debugging
    pid_t pid = getpid();
    printf("Pid: %d\n", pid);

    // Create the pwmgr class instance, connect the callbacks, and launch it
    Pwmgr *p = pwmgr_init();

    int status = g_application_run(G_APPLICATION(p->app), 0, NULL);

    // Free up C structs
    // TODO make the function below that frees up any
    // leftover memory
    // pwmgr_cleanup(p);

    return status;
}

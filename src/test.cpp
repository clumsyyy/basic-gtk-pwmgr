#include <cassert>
#include <iostream>
#include <string>

#include "queries.h"
#include <sqlite3.h>

std::string dbPath = ":memory:";

bool countTables(sqlite3 *db);

int main(void) {
    sqlite3 *db;
    int result = sqlite3_open(dbPath.c_str(), &db);
    assert(result == SQLITE_OK);

    handleDbSchema(db);

    sqlite3_close(db);

    // assert(countTables(db) != false);

    std::cout << "All test passed" << std::endl;
}

bool countTables(sqlite3 *db) {
    if (!db) {
        std::cout << "db param is null\n";
        return false;
    }
    sqlite3_stmt *stmt;
    int result = sqlite3_prepare_v2(db, qry_select_master_pw.c_str(),
                                    qry_select_master_pw.size(), &stmt, NULL);
    if (result != SQLITE_OK)
        return false;

    while (result != SQLITE_DONE) {
    }
    return false;
}

#ifndef PWMGR_ACCOUNT_HPP
#define PWMGR_ACCOUNT_HPP

#include <gtk/gtk.h>
#include <sqlite3.h>

typedef struct Account {
    int id;
    GString *name;
    GString *username;
    GString *pw;
    GString *desc;
} Account;

#endif

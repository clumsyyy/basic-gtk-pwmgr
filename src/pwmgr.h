#ifndef PWMGR_MAIN_H
#define PWMGR_MAIN_H

#include <gtk/gtk.h>
#include <iostream>
#include <sqlite3.h>

class Pwmgr {
  public:
    // gtk fields
    GtkWidget *mainWindow;     // main application window
    GtkApplication *app;       // main gtkapplication pointer
    GtkWidget *listBox;        // main list box pointer
    GtkWidget *scrolledWindow; // not needed?

    // fields for writing data
    std::string dataDir;
    sqlite3 *db;

    // Hashed password holder
    std::string masterPasswordHash;
    std::string masterPasswordSalt;

    // constructor
    Pwmgr(GtkApplication *, std::string, sqlite3 *);

    // gtk signal handlers
    static void activateSignalHandler(GtkApplication *, gpointer);
    static void showAddAccountForm(GtkButton *, gpointer);
    static void submitAddAccountForm(GtkButton *, gpointer);
    static void refreshListHandler(GtkButton *, gpointer);
    static void delBtnHandler(GtkButton *, gpointer);
    static void firstPasswordSaveHandler(GtkButton *, gpointer);

    // gtk startup functions
    static void normalStartup(Pwmgr *);
    static void firstStartup(Pwmgr *);
};

#endif

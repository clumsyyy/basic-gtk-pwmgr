#ifndef PWMGR_QUERIES_H
#define PWMGR_QUERIES_H

#include <sqlite3.h>

extern const char *qry_schema;

extern const char *qry_select_master_pw;
extern const char *qry_insert_master_pw;

extern const char *qry_get_account_by_id;
extern const char *qry_get_account_by_name;
extern const char *qry_get_accounts_all;
extern const char *qry_get_accounts_name_all;
extern const char *qry_insert_account;
extern const char *qry_delete_account;
extern const char *qry_update_accounts;

extern const char *qry_select_table_count;

#endif

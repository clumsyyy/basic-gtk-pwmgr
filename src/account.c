#include "account.h"
#include "queries.h"

std::vector<Account> getAccounts(sqlite3 *db) {
    std::vector<Account> vec;
    sqlite3_stmt *stmt;
    int result = sqlite3_prepare_v2(db, qry_get_accounts_all.c_str(),
                                    qry_get_accounts_all.size(), &stmt, NULL);
    if (result != SQLITE_OK) {
        std::cout << "result = " << result
                  << ", sqlite error: " << sqlite3_errmsg(db) << std::endl;
    }

    while (result != SQLITE_DONE) {
        result = sqlite3_step(stmt);
        if (result == SQLITE_ROW) {
            Account a;
            a.id = sqlite3_column_int(stmt, 0);
            a.name = std::string((char *)sqlite3_column_text(stmt, 1));
            a.username = std::string((char *)sqlite3_column_text(stmt, 2));
            a.pw = std::string((char *)sqlite3_column_text(stmt, 3));
            a.desc = std::string((char *)sqlite3_column_text(stmt, 4));
            vec.push_back(a);
        }
        std::cout << "result = " << result
                  << ", sqlite error: " << sqlite3_errmsg(db) << std::endl;
    }

    return vec;
}

Account::Account() {}
Account::Account(std::string n, std::string u) : name(n), username(u) {}
